<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login','index', 'index-clientes', 'index-tecnicos', 'index-admins', 'perfil'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'index-clientes', 'index-tecnicos', 'index-admins', 'perfil'],
                        'roles' => ['admin'],
                    ],
                ]   
            ]
            
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPerfil() {
        $usuario_id = Yii::$app->user->id; //SACA ID DEL USUARIO CONECTADO
        if (is_null($usuario_id)) {
            return $this->redirect(['site/login']);
        }
        $conectado = \app\models\Usuario::findOne($usuario_id);

        if (is_null($conectado)) {
            return $this->redirect(['site/login']);
        }

        if ($conectado->load(Yii::$app->request->post()) && $conectado->save()) {
            
        }

        return $this->render('perfil', [
                    'conectado' => $conectado,
        ]);
    }

    public function actionIndexClientes() {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->searchClientes(Yii::$app->request->queryParams);

        return $this->render('index-clientes', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexAdmins() {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->searchAdmins(Yii::$app->request->queryParams);

        return $this->render('index-admins', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexTecnicos() {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->searchTecnicos(Yii::$app->request->queryParams);

        return $this->render('index-tecnicos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Usuario();
        $auth = Yii::$app->authManager;
        $tecnico =  $auth->createRole("tecnico");
        if ($model->load(Yii::$app->request->post())) {
            $model->Tipo_Usuario_idTipo_Usuario = 2;
            $model->Estado_Usuario_idEstado_Usuario = 1;
            $auth->assign($tecnico, $model->Usuario);
            $model->save();
            return $this->redirect(['view', 'id' => $model->Usuario]);
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionCreateAdmin() {
        $model = new Usuario();
        $auth = Yii::$app->authManager;
        $admin =  $auth->createRole("admin");
        if ($model->load(Yii::$app->request->post())) {
            $model->Tipo_Usuario_idTipo_Usuario = 1;
            $model->Estado_Usuario_idEstado_Usuario = 1;
            $auth->assign($admin, $model->Usuario);
            $model->save();
            return $this->redirect(['view', 'id' => $model->Usuario]);
        }

        return $this->render('create-admin', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->Usuario]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
