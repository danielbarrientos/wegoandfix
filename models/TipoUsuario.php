<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Tipo_Usuario".
 *
 * @property int $idTipo_Usuario
 * @property string $Descripcion
 */
class TipoUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Tipo_Usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idTipo_Usuario', 'Descripcion'], 'required'],
            [['idTipo_Usuario'], 'integer'],
            [['Descripcion'], 'string', 'max' => 15],
            [['idTipo_Usuario'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTipo_Usuario' => 'Id Tipo Usuario',
            'Descripcion' => 'Descripcion',
        ];
    }
}
