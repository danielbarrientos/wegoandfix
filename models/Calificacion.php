<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calificacion".
 *
 * @property int $idCalificación
 * @property int $Calificacion
 * @property string $Comentario
 * @property string $Calificado_ID
 * @property string $Calificador_ID
 *
 * @property Usuario $calificador
 * @property Usuario $calificado
 */
class Calificacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calificacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Calificacion', 'Comentario', 'Calificado_ID', 'Calificador_ID'], 'required'],
            [['Calificacion'], 'integer'],
            [['Comentario'], 'string', 'max' => 100],
            [['Calificado_ID', 'Calificador_ID'], 'string', 'max' => 15],
            [['Calificador_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['Calificador_ID' => 'Usuario']],
            [['Calificado_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['Calificado_ID' => 'Usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCalificación' => 'Id Calificación',
            'Calificacion' => 'Calificacion',
            'Comentario' => 'Comentario',
            'Calificado_ID' => 'Calificado ID',
            'Calificador_ID' => 'Calificador ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalificador()
    {
        return $this->hasOne(Usuario::className(), ['Usuario' => 'Calificador_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalificado()
    {
        return $this->hasOne(Usuario::className(), ['Usuario' => 'Calificado_ID']);
    }
}
