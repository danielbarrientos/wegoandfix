<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property string $Usuario
 * @property string $Contrasena
 * @property string $Nombre
 * @property string $Apellidos
 * @property string $Fono
 * @property string $Correo
 * @property string $Direccion
 * @property int $Tipo_Usuario_idTipo_Usuario
 * @property int $Estado_Usuario_idEstado_Usuario
 *
 * @property Calificacion[] $calificacions
 * @property DetalleSolicitud[] $detalleSolicituds
 * @property EstadoUsuario $estadoUsuarioIdEstadoUsuario
 * @property TipoUsuario $tipoUsuarioIdTipoUsuario
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    const TIPO_USUARIO = [
        'CLIENTE'=>3,
        'TECNICO'=>2,
        'ADMINISTRADOR'=>1
    ];
    const ESTADO_USUARIO = [
        'DISPONIBLE'=>1,
        'OCUPADO'=>2,
        'BLOQUEADO'=>3
    ];
    
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Usuario', 'Contrasena', 'Nombre', 'Apellidos', 'Fono', 'Correo', 'Direccion', 'Tipo_Usuario_idTipo_Usuario', 'Estado_Usuario_idEstado_Usuario'], 'required'],
            [['Tipo_Usuario_idTipo_Usuario', 'Estado_Usuario_idEstado_Usuario'], 'integer'],
            [['Usuario'], 'string', 'max' => 15],
            [['Contrasena'], 'string', 'max' => 60],
            [['Nombre', 'Apellidos'], 'string', 'max' => 40],
            [['Fono', 'Correo'], 'string', 'max' => 45],
            [['Direccion'], 'string', 'max' => 20],
            [['Usuario'], 'unique'],
            [['Estado_Usuario_idEstado_Usuario'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoUsuario::className(), 'targetAttribute' => ['Estado_Usuario_idEstado_Usuario' => 'idEstado_Usuario']],
            [['Tipo_Usuario_idTipo_Usuario'], 'exist', 'skipOnError' => true, 'targetClass' => TipoUsuario::className(), 'targetAttribute' => ['Tipo_Usuario_idTipo_Usuario' => 'idTipo_Usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Usuario' => 'Usuario',
            'Contrasena' => 'Contraseña',
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'Fono' => 'Fono',
            'Correo' => 'Correo',
            'Direccion' => 'Direccion',
            'Tipo_Usuario_idTipo_Usuario' => 'Tipo Usuario Id Tipo Usuario',
            'Estado_Usuario_idEstado_Usuario' => 'Estado Usuario Id Estado Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalificacions()
    {
        return $this->hasMany(Calificacion::className(), ['Usuario_Usuario' => 'Usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleSolicituds()
    {
        return $this->hasMany(DetalleSolicitud::className(), ['Usuario_Usuario' => 'Usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoUsuarioIdEstadoUsuario()
    {
        return $this->hasOne(EstadoUsuario::className(), ['idEstado_Usuario' => 'Estado_Usuario_idEstado_Usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoUsuarioIdTipoUsuario()
    {
        return $this->hasOne(TipoUsuario::className(), ['idTipo_Usuario' => 'Tipo_Usuario_idTipo_Usuario']);
    }
    
    
    public function getAuthKey(): string {
        return $this->Nombre;
    }

    public function getId() {
        return $this->Usuario;
    }
    
    public static function findByUsername($username)
    {
        $usuario = Usuario::find()->where(['Usuario'=>$username])->one();
        if($usuario != null){
            return new static($usuario);
        }

        return null;
    }

    public function validateAuthKey($authKey): bool {
        return $this->Usuario === $authKey;
    }

    public static function findIdentity($id): \yii\web\IdentityInterface {
        $usuario = Usuario::findOne($id);
        if($usuario != null){
            return new static($usuario);
        }
        return null;
    }

    public static function findIdentityByAccessToken($token, $type = null): \yii\web\IdentityInterface {
        foreach (self::$users as $user) {
            if ($user['Contrasena'] === $token) {
                return new static($user);
            }
        }

        return null;
    }
    
    public function validatePassword($password)
    {
        return $this->Contrasena === $password;
    }
}
