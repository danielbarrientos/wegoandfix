<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetalleSolicitud;

/**
 * DetalleSolicitudSearch represents the model behind the search form of `app\models\DetalleSolicitud`.
 */
class DetalleSolicitudSearch extends DetalleSolicitud
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_DS', 'Precio', 'Solicitud_idSolicitud'], 'integer'],
            [['Fecha', 'Diagnostico', 'Usuario_Usuario'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleSolicitud::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_DS' => $this->id_DS,
            'Fecha' => $this->Fecha,
            'Precio' => $this->Precio,
            'Solicitud_idSolicitud' => $this->Solicitud_idSolicitud,
        ]);

        $query->andFilterWhere(['like', 'Diagnostico', $this->Diagnostico])
            ->andFilterWhere(['like', 'Usuario_Usuario', $this->Usuario_Usuario]);

        return $dataProvider;
    }
}
