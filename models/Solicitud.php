<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud".
 *
 * @property int $idSolicitud
 * @property string $Fecha
 * @property string $Descripcion
 * @property int $Estado_idEstado
 * @property string $Solicitadopor_Usuario
 * @property string $Atendidopor_Usuario
 *
 * @property DetalleSolicitud[] $detalleSolicituds
 * @property Estado $estadoIdEstado
 * @property Usuario $solicitadoporUsuario
 * @property Usuario $atendidoporUsuario
 */
class Solicitud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitud';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Fecha', 'Descripcion', 'Estado_idEstado', 'Solicitadopor_Usuario'], 'required'],
            [['Fecha'], 'safe'],
            [['Estado_idEstado'], 'integer'],
            [['Descripcion'], 'string', 'max' => 45],
            [['Solicitadopor_Usuario', 'Atendidopor_Usuario'], 'string', 'max' => 15],
            [['Estado_idEstado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['Estado_idEstado' => 'idEstado']],
            [['Solicitadopor_Usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['Solicitadopor_Usuario' => 'Usuario']],
            [['Atendidopor_Usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['Atendidopor_Usuario' => 'Usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idSolicitud' => 'Id Solicitud',
            'Fecha' => 'Fecha',
            'Descripcion' => 'Descripcion',
            'Estado_idEstado' => 'Estado Id Estado',
            'Solicitadopor_Usuario' => 'Solicitadopor Usuario',
            'Atendidopor_Usuario' => 'Atendidopor Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleSolicituds()
    {
        return $this->hasMany(DetalleSolicitud::className(), ['Solicitud_idSolicitud' => 'idSolicitud']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoIdEstado()
    {
        return $this->hasOne(Estado::className(), ['idEstado' => 'Estado_idEstado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitadoporUsuario()
    {
        return $this->hasOne(Usuario::className(), ['Usuario' => 'Solicitadopor_Usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtendidoporUsuario()
    {
        return $this->hasOne(Usuario::className(), ['Usuario' => 'Atendidopor_Usuario']);
    }
}
