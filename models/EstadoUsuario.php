<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_usuario".
 *
 * @property int $idEstado_Usuario
 * @property string $Descripcion
 *
 * @property Usuario[] $usuarios
 */
class EstadoUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEstado_Usuario', 'Descripcion'], 'required'],
            [['idEstado_Usuario'], 'integer'],
            [['Descripcion'], 'string', 'max' => 45],
            [['idEstado_Usuario'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEstado_Usuario' => 'Id Estado Usuario',
            'Descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['Estado_Usuario_idEstado_Usuario' => 'idEstado_Usuario']);
    }
}
