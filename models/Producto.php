<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $id
 * @property string $codigo
 * @property string $nombre
 * @property int $precio
 * @property int $categoria_id
 * @property int $cantidad
 *
 * @property Categoria $categoria
 * @property ProductoVenta[] $productoVentas
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'nombre', 'categoria_id'], 'required'],
            [['precio', 'categoria_id', 'cantidad'], 'integer'],
            [['codigo'], 'string', 'max' => 45],
            [['nombre'], 'string', 'max' => 100],
            [['categoria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['categoria_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Código',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'categoria_id' => 'Categoría',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categoria::className(), ['id' => 'categoria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductoVentas()
    {
        return $this->hasMany(ProductoVenta::className(), ['producto_id' => 'id']);
    }
}
