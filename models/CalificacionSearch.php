<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calificacion;

/**
 * CalificacionSearch represents the model behind the search form of `app\models\Calificacion`.
 */
class CalificacionSearch extends Calificacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCalificación', 'Calificacion'], 'integer'],
            [['Comentario', 'Calificado_ID', 'Calificador_ID'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$Calificado_ID)
    {
        
        $query = Calificacion::find()->where(['Calificado_ID'=>$Calificado_ID]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idCalificación' => $this->idCalificación,
            'Calificacion' => $this->Calificacion,
        ]);

        $query->andFilterWhere(['like', 'Comentario', $this->Comentario])
            ->andFilterWhere(['like', 'Calificado_ID', $this->Calificado_ID])
            ->andFilterWhere(['like', 'Calificador_ID', $this->Calificador_ID]);

        return $dataProvider;
    }
}
