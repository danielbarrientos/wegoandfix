<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detalle_solicitud".
 *
 * @property int $id_DS
 * @property string $Fecha
 * @property int $Precio
 * @property string $Diagnostico
 * @property string $Usuario_Usuario
 * @property int $Solicitud_idSolicitud
 *
 * @property Solicitud $solicitudIdSolicitud
 * @property Usuario $usuarioUsuario
 */
class DetalleSolicitud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detalle_solicitud';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_DS', 'Fecha', 'Precio', 'Diagnostico', 'Usuario_Usuario', 'Solicitud_idSolicitud'], 'required'],
            [['id_DS', 'Precio', 'Solicitud_idSolicitud'], 'integer'],
            [['Fecha'], 'safe'],
            [['Diagnostico'], 'string', 'max' => 200],
            [['Usuario_Usuario'], 'string', 'max' => 15],
            [['id_DS'], 'unique'],
            [['Solicitud_idSolicitud'], 'exist', 'skipOnError' => true, 'targetClass' => Solicitud::className(), 'targetAttribute' => ['Solicitud_idSolicitud' => 'idSolicitud']],
            [['Usuario_Usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['Usuario_Usuario' => 'Usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_DS' => 'Id Ds',
            'Fecha' => 'Fecha',
            'Precio' => 'Precio',
            'Diagnostico' => 'Diagnostico',
            'Usuario_Usuario' => 'Usuario Usuario',
            'Solicitud_idSolicitud' => 'Solicitud Id Solicitud',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudIdSolicitud()
    {
        return $this->hasOne(Solicitud::className(), ['idSolicitud' => 'Solicitud_idSolicitud']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioUsuario()
    {
        return $this->hasOne(Usuario::className(), ['Usuario' => 'Usuario_Usuario']);
    }
}
