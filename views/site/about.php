<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Gestion Tecnicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="<?=Url::to(['usuario/index', ])?>">Ver Tecnicos</a></p>
    </div>
</div>
 