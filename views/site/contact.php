<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Mi perfil';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1>Mi Perfil</h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Datos Actualizados Correctamente
        </div>

       

    <?php else: ?>

        <p>
            Modifica tus datos personales
        </p>

        <div class="row">
            <div class="col-lg-5">
                
                <!--
                $form->field($model, 'name')->textInput(['autofocus' => true])
                $form->field($model, 'email')
                $form->field($model, 'subject')
                $form->field($model, 'body')->textarea(['rows' => 6])
                $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ])
                -->
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                
                    <?= $conectado->Nombre." ".$conectado->Apellidos ?>
                    

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
