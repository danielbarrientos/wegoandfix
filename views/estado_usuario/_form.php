<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEstado_Usuario')->textInput() ?>

    <?= $form->field($model, 'Descripcion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
