<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoUsuario */

$this->title = 'Update Estado Usuario: ' . $model->idEstado_Usuario;
$this->params['breadcrumbs'][] = ['label' => 'Estado Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEstado_Usuario, 'url' => ['view', 'id' => $model->idEstado_Usuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estado-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
