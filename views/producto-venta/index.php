<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoVentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Producto Ventas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-venta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Producto Venta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cantidad',
            'venta_id',
            'producto_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
