<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idSolicitud') ?>

    <?= $form->field($model, 'Fecha') ?>

    <?= $form->field($model, 'Descripcion') ?>

    <?= $form->field($model, 'Estado_idEstado') ?>

    <?= $form->field($model, 'Solicitadopor_Usuario') ?>

    <?php // echo $form->field($model, 'Atendidopor_Usuario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
