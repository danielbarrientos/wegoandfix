<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\LoginForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            // Yii::$app->user->isGuest ES PARA VERIFICAR SI ESTA CONECTADO ALGUN USUARIO

            /* if (Yii::$app->user->isGuest) {

              } */
            ?>
            <?php
            /* if(Yii::$app->user->isGuest){

              } */
            $itemsLogueado = [
                ['label' => 'Solicitudes', 'url' => ['/solicitud/index']],
                ['label' => 'Agregar Administrador', 'url' => ['/usuario/index-admins']],
                ['label' => 'Gestion Clientes', 'url' => ['/usuario/index-clientes']],
                ['label' => 'Gestion Tecnicos', 'url' => ['/usuario/index-tecnicos']],
                ['label' => 'Mi Perfil','url' => ['/usuario/perfil']],
                Yii::$app->user->isGuest ? (
                ['label' => 'Iniciar Sesion', 'url' => ['/site/login']]
                ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                        'Cerrar Sesión (' . Yii::$app->user->identity->Nombre . ')', ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
                )
            ];

            $itemsNoLogueado = [
                Yii::$app->user->isGuest ? (
                ['label' => 'Iniciar Sesion', 'url' => ['/site/login']]
                ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                        'Cerrar Sesión (' . Yii::$app->user->identity->Nombre . ')', ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
                )
            ];
            //MEJORAR ESTA WA
            
            if(Yii::$app->user->isGuest){
                $items = $itemsNoLogueado;
            }else{
                $items = $itemsLogueado;
            }
            
            /*
            $model = new LoginForm();
            if (Yii::$app->user->isGuest) {
                $items = $itemsNoLogueado;
                
            } else if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $usuario_id = Yii::$app->user->id;
                $conectado = app\models\Usuario::findOne($usuario_id);
                if ($conectado->Tipo_Usuario_idTipo_Usuario = 1//Admin) {
                    $items = $itemsLogueado;
                } else {
                    $items = $itemsNoLogueado;
                }
            }*/
            //HASTA ACA XD
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $items,
            ]);

            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
