<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DetalleSolicitudSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detalle Solicituds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-solicitud-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Detalle Solicitud', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_DS',
            'Fecha',
            'Precio',
            'Diagnostico',
            'Usuario_Usuario',
            //'Solicitud_idSolicitud',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
