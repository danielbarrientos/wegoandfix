<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleSolicitudSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-solicitud-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_DS') ?>

    <?= $form->field($model, 'Fecha') ?>

    <?= $form->field($model, 'Precio') ?>

    <?= $form->field($model, 'Diagnostico') ?>

    <?= $form->field($model, 'Usuario_Usuario') ?>

    <?php // echo $form->field($model, 'Solicitud_idSolicitud') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
