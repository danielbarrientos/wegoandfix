<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleSolicitud */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-solicitud-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_DS')->textInput() ?>

    <?= $form->field($model, 'Fecha')->textInput() ?>

    <?= $form->field($model, 'Precio')->textInput() ?>

    <?= $form->field($model, 'Diagnostico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Usuario_Usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Solicitud_idSolicitud')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
