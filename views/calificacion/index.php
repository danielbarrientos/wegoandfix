<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Calificacion;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalificacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Calificacion';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calificacion-index">
    <div class="row">
        <div class="col-md-3">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-6"></div>
        <div class="col-md-3">
        </div>
    </div>
    
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'idCalificación',
            'Calificacion',
            'Comentario',
            'Calificado_ID',
            'Calificador_ID',
        ],
    ]); ?>


</div>
