<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calificacion */
/* @var $form ActiveForm */
?>
<div class="calificacion">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'Calificacion') ?>
        <?= $form->field($model, 'Comentario') ?>
        <?= $form->field($model, 'Calificado_ID') ?>
        <?= $form->field($model, 'Calificador_ID') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- calificacion -->
