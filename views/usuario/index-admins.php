<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gestion de Administradores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Administrador', ['create-admin'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Usuario',
            'Nombre',
            'Apellidos',
            'Fono',
            'Correo',
            'Direccion',
            ['attribute'=>'Tipo_Usuario_idTipo_Usuario','value'=>
                function($data){
                    return $data->tipoUsuarioIdTipoUsuario->Descripcion;
                }    
            ],
            ['attribute'=>'Estado_Usuario_idEstado_Usuario','value'=>
                function($data){
                    return $data->estadoUsuarioIdEstadoUsuario->Descripcion;
                }    
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>


</div>
