<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gestion de Tecnicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Técnico', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Usuario',
            //'Contrasena',
            'Nombre',
            'Apellidos',
            'Fono',
            'Correo',
            'Direccion',
            ['attribute'=>'Tipo_Usuario_idTipo_Usuario','value'=>
                function($data){
                    return $data->tipoUsuarioIdTipoUsuario->Descripcion;
                }    
            ],
            ['attribute'=>'Estado_Usuario_idEstado_Usuario','value'=>
                function($data){
                    return $data->estadoUsuarioIdEstadoUsuario->Descripcion;
                }    
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {ver_calificacion}',
                'buttons' => [
                    'ver_calificacion' => function($url, $model, $key) {     
                        return Html::a("calificación",['calificacion/index','Calificado_ID'=>$model->Usuario]);
                    }
                ]
            ],
            
        ],
    ]); ?>


</div>
