<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">
<!--$form->field($model, 'Apellidos')->textInput(['maxlength' => true])
    $form->field($model, 'Usuario')->textInput(['maxlength' => true])
    $form->field($model, 'Nombre')->textInput(['maxlength' => true])
    $form->field($model, 'Fono')->textInput(['maxlength' => true])
    $form->field($model, 'Correo')->textInput(['maxlength' => true])
    $form->field($model, 'Direccion')->textInput(['maxlength' => true])
-->
    <?php $form = ActiveForm::begin(); ?>


    <?= 
    $form->field($model, 'Tipo_Usuario_idTipo_Usuario')->dropDownList([
        1 => 'ADMINISTRADOR',
        2 => 'TECNICO',
        3 => 'CLIENTE',
        ], ['prompt' => 'SELECCIONE USUARIO']);
    ?>
    <?= 
    $form->field($model, 'Estado_Usuario_idEstado_Usuario')->dropDownList([
        1 => 'DISPONIBLE',  
        2 => 'OCUPADO',
        3 => 'BLOQUEADO',
        ], ['prompt' => 'SELECCIONE ESTADO']);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
