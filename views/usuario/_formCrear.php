<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">
<!--$form->field($model, 'Apellidos')->textInput(['maxlength' => true])
    $form->field($model, 'Usuario')->textInput(['maxlength' => true])
    $form->field($model, 'Nombre')->textInput(['maxlength' => true])
    $form->field($model, 'Fono')->textInput(['maxlength' => true])
    $form->field($model, 'Correo')->textInput(['maxlength' => true])
    $form->field($model, 'Direccion')->textInput(['maxlength' => true])
-->
    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'Usuario')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'Contrasena')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'Nombre')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'Apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Fono')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'Correo')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'Direccion')->textInput(['maxlength' => true])?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
