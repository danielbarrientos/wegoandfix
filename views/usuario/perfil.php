<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-contact">
    <h1>Mi Perfil</h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Datos Actualizados Correctamente
        </div>

        <p>
            Note that if you turn on the Yii debugger, you should be able
            to view the mail message on the mail panel of the debugger.
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Because the application is in development mode, the email is not sent but saved as
                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                application component to be false to enable email sending.
            <?php endif; ?>
        </p>

    <?php else: ?>

        <p>
            Modifica tus datos personales
        </p>

        <div class="row">
            <div class="col-lg-5">

                <!--
                $form->field($model, 'name')->textInput(['autofocus' => true])
                $form->field($model, 'email')
                $form->field($model, 'subject')
                $form->field($model, 'body')->textarea(['rows' => 6])
                $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ])
                -->
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                
                <?=$form->field($conectado, 'Nombre')->textInput(['autofocus' => true]);?>
                <?=$form->field($conectado, 'Apellidos')->textInput(['autofocus' => true]);?>
                <?=$form->field($conectado, 'Fono')->textInput(['autofocus' => true]);?>
                <?=$form->field($conectado, 'Correo')->textInput(['autofocus' => true]);?>
                <?=$form->field($conectado, 'Direccion')->textInput(['autofocus' => true]);?>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div
                    </div> 
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
