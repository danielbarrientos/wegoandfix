<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = $model->Usuario;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->Usuario], ['class' => 'btn btn-primary']) ?>
        <!--Html::a('Delete', ['delete', 'id' => $model->Usuario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Usuario',
            //'Contrasena',
            'Nombre',
            'Apellidos',
            'Fono',
            'Correo',
            'Direccion',
            ['attribute'=>'Tipo_Usuario_idTipo_Usuario','value'=>
                function($data){
                    return $data->tipoUsuarioIdTipoUsuario->Descripcion;
                }    
            ],
            ['attribute'=>'Estado_Usuario_idEstado_Usuario','value'=>
                function($data){
                    return $data->estadoUsuarioIdEstadoUsuario->Descripcion;
                }    
            ],
        ],
    ]) ?>

</div>
